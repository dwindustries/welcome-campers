"use strict";

import heroAnimation from './libs/heroAnimation';
import stickyHeader from './libs/stickyHeader';
import Dropdown from './libs/dropdown';
import animatePaths from './libs/animatePaths';
import InstaFetch from './libs/instaFetch';

document.addEventListener("DOMContentLoaded", function(event) { 

  Dropdown('.js-nav-trigger','.js-nav-items');
  InstaFetch();

});

window.onload = () => {
    window.requestAnimationFrame(heroAnimation);
    window.onscroll = function() { stickyHeader() };
    animatePaths(); // Bee animations

        // Output Date Year
    const dateEl = document.querySelector('.js-date'),
          date = new Date(),
          year = date.getFullYear();
    dateEl.innerHTML = year;
}
export default function animatePaths() {
    // Create a path `Object`
    var path1 = anime.path('.bee-path #Path-1');
    var motionPath = anime({
        targets: '.bee-path .bee1',
        translateX: path1('x'),
        translateY: path1('y'),
        rotate: path1('angle'),
        easing: 'linear',
        duration: 15000,
        loop: true
    });

    var path2 = anime.path('.bee-path #Path-2');
    var motionPath = anime({
        targets: '.bee-path .bee2',
        translateX: path2('x'),
        translateY: path2('y'),
        rotate: path2('angle'),
        easing: 'linear',
        duration: 12000,
        loop: true
    });

    var path2 = anime.path('.bee-path #Path-3');
    var motionPath = anime({
        targets: '.bee-path .bee3',
        translateX: path2('x'),
        translateY: path2('y'),
        rotate: path2('angle'),
        easing: 'linear',
        duration: 12000,
        loop: true
    });
}
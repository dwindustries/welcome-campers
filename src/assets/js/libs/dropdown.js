import {hasClass, addClass, removeClass} from './classNameUtils';

// Primary Navigation function
export default function Dropdown(trigger, content) {
  const dropdownTrigger = document.querySelector(trigger);
  const dropdownItems = document.querySelector(content);
  const navItems = document.getElementsByClassName('js-smooth-scroll');

  if (window.innerWidth < 900) {
    addClass(dropdownItems,'closed');

    for(let i = 0; i < navItems.length; i++) {
        navItems[i].addEventListener("click", closeDropdown, false);
        navItems[i].addEventListener("touch", closeDropdown, false);
    }
  }

  function closeDropdown() {
    addClass(dropdownItems, 'closed');
    removeClass(dropdownTrigger,'active');
  }

  // Slide Nav up and Down with CSS class & calculated height
  dropdownTrigger.addEventListener("click", () => {
    if(hasClass(dropdownItems, 'closed') == true){
      dropdownItems.style.height = dropdownItems.scrollHeight + 'px';
      removeClass(dropdownItems, 'closed');
      addClass(dropdownTrigger,'active');
    }
    else {
        closeDropdown();
    }
  }, false);

  window.addEventListener('resize', () => {
    removeClass(dropdownTrigger,'active');
    dropdownItems.style.height = 'auto';

    if (window.innerWidth < 900) {
      addClass(dropdownItems,'closed');
    }
    else {
      removeClass(dropdownItems, 'closed');
      dropdownItems.style.height = dropdownItems.scrollHeight + 'px';
    }
  }, false);
  

};
export default function heroAnimation() {
    const heroEls = document.getElementsByClassName('js-hero');
  
    // Switch on anim imgs with js
    for(let i = 0; i < heroEls.length; i++) {
        heroEls[i].style.display = 'block';
    }

    if(window.innerWidth >= 1100 && window.innerWidth < 1780) {
        const heroEl1 = document.getElementById('js-hero-el1'),
            heroEl2 = document.getElementById('js-hero-el2'),
            heroEl3 = document.getElementById('js-hero-el3'),
            heroEl4 = document.getElementById('js-hero-el4');

        setInterval(() => {
            setTimeout(() => {
              heroEl1.style.opacity = 0;
              heroEl2.style.opacity = 1;
            }, 100);
            
            setTimeout(() => {
                heroEl2.style.opacity = 0;
                heroEl3.style.opacity = 1;
            }, 350);
        
            setTimeout(() => {
                heroEl3.style.opacity = 0;
                heroEl4.style.opacity = 1;
            }, 600);
        
            // Reverse       
            setTimeout(() => {
                heroEl4.style.opacity = 0;
                heroEl3.style.opacity = 1;
            }, 850);
        
            setTimeout(() => {
                heroEl3.style.opacity = 0;
                heroEl2.style.opacity = 1;
            }, 1100);
        
            setTimeout(() => {
                heroEl2.style.opacity = 0;
                heroEl1.style.opacity = 1;
            }, 1350); 
        }, 1500);  

    } else if (window.innerWidth < 1100) {
        const heroEl1 = document.getElementById('js-hero-mobile-el1'),
            heroEl2 = document.getElementById('js-hero-mobile-el2');

        setInterval(() => {
            setTimeout(() => {
              heroEl1.style.opacity = 0;
              heroEl2.style.opacity = 1;
            }, 100);
        
            // Reverse
            setTimeout(() => {
                heroEl2.style.opacity = 0;
                heroEl1.style.opacity = 1;
            }, 300); 
        }, 400);  
        
    }

  }
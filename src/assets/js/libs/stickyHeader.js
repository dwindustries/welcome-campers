export default function stickyHeader() {
    const header = document.querySelector(".js-site-header");
    const trigger = document.getElementById("page");

    // Get the offset position of the navbar
    let sticky = trigger.offsetTop;

    if (window.pageYOffset > sticky) {
        trigger.classList.add("sticky-offset");
        header.classList.add("sticky");
    } else {
        trigger.classList.remove("sticky-offset");
        header.classList.remove("sticky");
    }
}